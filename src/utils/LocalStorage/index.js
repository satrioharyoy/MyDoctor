import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert} from 'react-native'

export const storeData = async (key, value) => {
    try {
      const jsonValue = JSON.stringify(value)
      await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
      // saving error
      Alert.alert('Failed to store data on Local Storage')
    }
}


export const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        // value previously stored
        return  JSON.parse(value)
      }
    } catch(e) {
      // error reading value
      Alert.alert('Failed to get data from Local Storage')
    }
  }
  