import Doctor1 from './Doctor1.png';
import Doctor2 from './Doctor2.png';
import Doctor3 from './Doctor3.png';
import DummyProfile from './DummyProfile1.png';
import HospitalBackground from './image_background_hospital.png';
import Hospital1 from './image_hospital1.png';
import Hospital2 from './image_hospital2.png';
import Hospital3 from './image_hospital3.png';
import News1 from './News1.png';
import News2 from './News2.png';
import News3 from './News3.png';


export {
    DummyProfile,
    Doctor1,
    Doctor2,
    Doctor3,
    News1,
    News2,
    News3,
    HospitalBackground,
    Hospital1,
    Hospital3,
    Hospital2,
};

