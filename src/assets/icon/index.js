import IconBackDark from './arrow_back_24px.svg';
import IconAddPhoto from './icon_btn_add_photo.svg';
import IconRemovePhoto from './icon_btn_remove_photo.svg';
import IconHomeInactive from './icon_home_inactive.svg';
import IconHomeActive from './icon_home_active.svg';
import IconMessagesInactive from './icon_messages_inactive.svg';
import IconMessagesActive from './icon_messages_active.svg';
import IconHospitalsInactive from './icon_hospitals_inactive.svg';
import IconHospitalsActive from './icon_hospitals_active.svg';
import IconStar from './icon_star.svg';
import IconBackLight from './arrow_back_light.svg';
import IconNext from './icon_next.svg';
import IconSendDark from './icon_send_dark.svg';
import IconSendLight from './icon_send_light.svg';
import IconEditProfile from './icon_edit_profile.svg';
import IconLanguage from './icon_language.svg';
import IconGiveRate from './icon_give_rate.svg';
import IconHelpCenter from './icon_help_center.svg';
import IconFemale from './icon_female.svg';
import IconMale from './icon_male.svg';

export {
  IconBackDark,
  IconAddPhoto,
  IconRemovePhoto,
  IconHomeInactive,
  IconHomeActive,
  IconMessagesInactive,
  IconMessagesActive,
  IconHospitalsInactive,
  IconHospitalsActive,
  IconStar,
  IconBackLight,
  IconNext,
  IconSendDark,
  IconSendLight,
  IconEditProfile,
  IconLanguage,
  IconGiveRate,
  IconHelpCenter,
  IconFemale,
  IconMale,
};
