import BGGetStarted from './background_get_started.png';
import ILLogo from './logo_my_doctor.svg';
import ILPhotoNull from './icon_user_photo_null.png';
import ILDokterAnak from './icon_dokter_anak.svg';
import ILDokterUmum from './icon_dokter_umum.svg';
import ILObat from './icon_obat.svg';
import ILPsikiater from './icon_psikiater.svg';

export {
  ILLogo,
  BGGetStarted,
  ILPhotoNull,
  ILDokterAnak,
  ILDokterUmum,
  ILObat,
  ILPsikiater,
};
