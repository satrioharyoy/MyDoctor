import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {colors, fonts} from '../../../utils';

const InputText = ({label, value, secureTextEntry, onChangeText, disable}) => {
  const [border, setBorder] = useState(colors.borders);
  const onFocusForm = () => {
    setBorder(colors.tertiary);
  };
  const onBlurForm = () => {
    setBorder(colors.borders);
  };
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        onFocus={onFocusForm}
        onBlur={onBlurForm}
        style={styles.inputText(border)}
        value={value}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        editable={!disable}
        selectTextOnFocus={!disable}
      />
    </View>
  );
};

export default InputText;

const styles = StyleSheet.create({
  inputText: (border) => ({
    borderRadius: 10,
    borderWidth: 1,
    borderColor: border,
    padding: 12,
  }),
  label: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    marginBottom: 6,
  },
});
