import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { colors,fonts } from '../../../utils';

const Link = ({title, fontSize, textAlign, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.linkText(fontSize, textAlign)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Link;

const styles = StyleSheet.create({
  linkText: (fontSize, textAlign) => ({
    fontSize: fontSize ? fontSize : 12,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    textDecorationLine: 'underline',
    textAlign: textAlign,
  }),
});
