import React from 'react';
import {StyleSheet} from 'react-native';
import {IconBackDark, IconBackLight} from '../../../assets/icon';

const IconOnly = ({icon}) => {
  switch (icon) {
    case 'iconBackDark':
      return <IconBackDark />;
    case 'iconBackLight':
      return <IconBackLight />;
    default:
      return <IconBackDark />;
  }
};

export default IconOnly;

const styles = StyleSheet.create({});
