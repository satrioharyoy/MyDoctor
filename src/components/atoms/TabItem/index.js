import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {
  IconHomeInactive,
  IconHomeActive,
  IconMessagesActive,
  IconMessagesInactive,
  IconHospitalsActive,
  IconHospitalsInactive,
} from '../../../assets/icon';
import {colors, fonts} from '../../../utils';

const Icon = ({title, actived}) => {
  if (title === 'HomeScreen') {
    return actived ? <IconHomeActive /> : <IconHomeInactive />;
  } else if (title === 'MessagesScreen') {
    return actived ? <IconMessagesActive /> : <IconMessagesInactive />;
  } else if (title === 'HospitalsScreen') {
    return actived ? <IconHospitalsActive /> : <IconHospitalsInactive />;
  } else return <IconHospitalsActive />;
};

const TabItem = ({title, onPress, onLongPress, actived}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon title={title} actived={actived} />
      <Text style={styles.textTab(actived)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  textTab: (actived) => ({
    textAlign: 'center',
    fontSize: 10,
    fontFamily: fonts.primary[600],
    color: actived ? colors.text.menuActive : colors.text.menuInactive,
    marginTop: 4,
  }),
});
