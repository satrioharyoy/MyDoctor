import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';
import HeaderProfile from './HeaderProfile';

const Header = ({headerTitle, onPress, icon, type}) => {
  if (type === 'darkProfile') {
    return <HeaderProfile onPress={onPress} />;
  }
  return (
    <View style={styles.container(type)}>
      <Button type="iconOnly" onPress={onPress} icon={icon} />
      <Text style={styles.headerTitle(type)}>{headerTitle}</Text>
      <Gap width={24} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: (type) => ({
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 30,
    alignItems: 'center',
    backgroundColor: type === 'dark' ? colors.secondary : colors.white,
    borderBottomLeftRadius: type === 'dark' ? 20 : 0,
    borderBottomRightRadius: type === 'dark' ? 20 : 0,
  }),
  headerTitle: (type) => ({
    flex: 1,
    textAlign: 'center',
    fontSize: 20,
    color: type === 'dark' ? colors.white : colors.text.primary,
    fontFamily: fonts.primary[600],
  }),
});
