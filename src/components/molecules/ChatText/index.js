import React from 'react'
import IsMe from './IsMe'
import Others from './Others'

const ChatText = ({isMe}) => {
    if (isMe) {
        return <IsMe />
    } else {
       return <Others />
    }
}


export default ChatText
