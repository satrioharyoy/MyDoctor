import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Doctor1} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Others = () => {
  return (
    <View style={styles.container}>
      <Image source={Doctor1} style={styles.avatar} />
      <View>
        <View style={styles.content}>
          <Text style={styles.chatContent}>
            Ibu dokter, apakah memakan jeruk tiap hari itu buruk?
          </Text>
        </View>
        <Text style={styles.timeText}>4.20 AM</Text>
      </View>
    </View>
  );
};

export default Others;

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    paddingHorizontal: 16,
    flexDirection: 'row',
  },
  content: {
    maxWidth: '80%',
    padding: 12,
    paddingRight: 18,
    backgroundColor: colors.primary,
    borderRadius: 10,
    borderBottomLeftRadius: 0,
  },
  chatContent: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
  timeText: {
    fontSize: 11,
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    marginTop: 8,
  },
  avatar:{
      width:30,
      height:30,
      borderRadius:30/2,
      marginRight:12,
      alignSelf:'flex-end'
  }
});
