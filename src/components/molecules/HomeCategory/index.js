import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ILDokterAnak, ILDokterUmum, ILObat, ILPsikiater} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Icon = ({category}) => {
  switch (category) {
    case 'Dokter Umum':
      return <ILDokterUmum style={styles.icon} />;
    case 'Psikiater':
      return <ILPsikiater style={styles.icon} />;
    case 'Obat':
      return <ILObat style={styles.icon} />;
    case 'Dokter Anak':
      return <ILDokterAnak style={styles.icon} />;
    default:
      return <ILDokterUmum style={styles.icon} />;
  }
};

const HomeCategory = ({category, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Icon category={category} />
      <Text style={styles.labelText}>Saya Butuh</Text>
      <Text style={styles.categoryText}>{category}</Text>
    </TouchableOpacity>
  );
};

export default HomeCategory;

const styles = StyleSheet.create({
  container: {
    padding: 12,
    backgroundColor: colors.cardLight,
    borderRadius: 10,
    alignSelf: 'flex-start',
    marginRight: 10,
    height: 130,
  },
  labelText: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.primary,
  },
  categoryText: {
    fontSize: 12,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  icon: {
    marginBottom: 28,
  },
});
