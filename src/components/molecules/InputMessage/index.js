import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { colors, fonts } from '../../../utils';
import { Button } from '../../atoms';

const InputMessage = ({disable}) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder="Tulis pesan untuk Nairobi"
      />
      <Button type="btnIconSend" disable={disable} />
    </View>
  );
};

export default InputMessage;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 26,
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  textInput: {
    backgroundColor: colors.disable,
    borderRadius: 10,
    padding: 14,
    fontFamily: fonts.primary[400],
    fontSize: 14,
    color: colors.text.primary,
    flex: 1,
    maxHeight: 45,
    marginRight: 10,
  },
});
