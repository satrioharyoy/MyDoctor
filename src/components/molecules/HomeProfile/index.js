import React,{useState, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors, fonts, getData} from '../../../utils';
import {ILPhotoNull} from '../../../assets'

const HomeProfile = ({onPress}) => {
  const [profile, setProfile] = useState({
    fullName : '',
    profession : '',
    photo : ILPhotoNull
  })

  useEffect(() => {
    getData('userData').then(res => {
      let data = res
      data.photo = {uri : res.photo}
      setProfile(res)
    })
  }, [])

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Image source={profile.photo} style={styles.avatar} />
      <View>
        <Text style={styles.profileName}>{profile.fullName}</Text>
        <Text style={styles.jobProfile}>{profile.profession}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default HomeProfile;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
    marginRight: 12,
  },
  profileName: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    textTransform : 'capitalize'
  },
  jobProfile: {
    fontSize: 12,
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    textTransform : 'capitalize'
  },
});
