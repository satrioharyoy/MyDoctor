import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IconFemale, IconMale, IconRemovePhoto} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Icon = ({icon}) => {
  switch (icon) {
    case 'removePhoto':
      return <IconRemovePhoto style={styles.removePhoto} />;
    case 'male':
      return <IconMale style={styles.removePhoto} />;
    case 'female':
      return <IconFemale style={styles.removePhoto} />;
    default:
      return <IconRemovePhoto style={styles.removePhoto} />;
  }
};

const Profile = ({name, desc, avatar, icon, isRemove, onPress}) => {
  return (
    <View style={styles.container}>
      {!isRemove && (
        <View style={styles.borderAvatar}>
          <Image source={avatar} style={styles.avatar} />
          {icon && <Icon icon={icon} />}
        </View>
      )}
      {isRemove && (
        <TouchableOpacity style={styles.borderAvatar} onPress={onPress}>
          <Image source={avatar} style={styles.avatar} />
          {icon && <Icon icon={icon} />}
        </TouchableOpacity>
      )}
      {name && (
        <View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.job}>{desc}</Text>
        </View>
      )}
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  avatar: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  borderAvatar: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    borderColor: colors.borders,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
    marginTop: 16,
    textAlign: 'center',
  },
  job: {
    fontFamily: fonts.primary[400],
    fontSize: 16,
    color: colors.text.secondary,
    marginTop: 2,
    textAlign: 'center',
  },
  removePhoto: {
    position: 'absolute',
    right: 4,
    bottom: 2,
  },
});
