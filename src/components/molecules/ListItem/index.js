import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  IconEditProfile,
  IconGiveRate,
  IconHelpCenter,
  IconLanguage,
  IconNext,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Icon = ({icon}) => {
  switch (icon) {
    case 'editProfile':
      return <IconEditProfile />;
    case 'language':
      return <IconLanguage />;
    case 'giveRate':
      return <IconGiveRate />;
    case 'helpCenter':
      return <IconHelpCenter />;
    default:
      return <IconEditProfile />;
  }
};

const ListItem = ({profile, title, desc, type, onPress, icon}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      {icon ? (
        <Icon icon={icon} />
      ) : (
        <Image source={profile} style={styles.avatar} />
      )}
      <View style={styles.itemSection}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.desc}>{desc}</Text>
      </View>
      {type === 'next' && <IconNext />}
    </TouchableOpacity>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  container: {
    padding: 16,
    borderBottomWidth: 1,
    borderColor: colors.borders,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemSection: {
    flex: 1,
    marginLeft:12
  },
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
  desc: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.secondary,
  },
});
