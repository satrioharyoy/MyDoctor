import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconStar } from '../../../assets';

const RatedDoctor = ({name, desc, avatar,onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Image source={avatar} style={styles.avatar} />
      <View style={styles.profileWrapper}>
        <Text>{name}</Text>
        <Text>{desc} </Text>
      </View>
      <View style={styles.ratedWrapper}>
        <IconStar />
        <IconStar />
        <IconStar />
        <IconStar />
        <IconStar />
      </View>
    </TouchableOpacity>
  );
};

export default RatedDoctor;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 16,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    marginRight: 12,
  },
  ratedWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
});
