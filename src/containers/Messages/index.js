import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Doctor1, Doctor2, Doctor3 } from '../../assets';
import { ListItem } from '../../components';
import { colors, fonts } from '../../utils';

const Messages = ({navigation}) => {
  const [doctors] = useState([
    {
      id: 1,
      profil: Doctor1,
      title: 'Alexander Jannie',
      messageText: 'Baik ibu, terima kasih banyak atas wakt...',
    },
    {
      id: 2,
      profil: Doctor2,
      title: 'Nairobi Putri Hayza',
      messageText: 'Oh tentu saja tidak karena jeruk it...',
    },
    {
      id: 3,
      profil: Doctor3,
      title: 'Nairobi Putri Hayza',
      messageText: 'Oke menurut pak dokter bagaimana unt...',
    },
  ]);
  return (
    <View style={styles.container}>
      <View style={styles.contentSection}>
        <Text style={styles.screenTitle}>Messages</Text>
        {doctors.map((doctor) => {
          return (
            <ListItem
              key={doctor.id}
              profile={doctor.profil}
              title={doctor.title}
              desc={doctor.messageText}
              onPress={()=>navigation.navigate('ChatDoctor')}
            />
          );
        })}
      </View>
    </View>
  );
};

export default Messages;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  contentSection: {
    flex: 1,
    backgroundColor: colors.white,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  screenTitle: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 30,
    marginHorizontal: 16,
  },
});
