import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {
  Hospital1,
  Hospital2,
  Hospital3,
  HospitalBackground,
} from '../../assets';
import {ListHospitals} from '../../components';
import {colors, fonts} from '../../utils';

const Hospitals = () => {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={HospitalBackground}
        style={styles.imageBackground}>
        <Text style={styles.screenTitle}>Nearby Hospitals</Text>
        <Text style={styles.hospitalCount}>3 Tersedia</Text>
      </ImageBackground>
      <View style={styles.contentSection}>
        <ListHospitals
          type="Rumah Sakit"
          name="Citra Bunga Merdeka"
          address="Jln. Surya Sejahtera 20"
          pic={Hospital1}
        />
        <ListHospitals
          type="Rumah Sakit"
          name="Happy Family & Kids"
          address="Jln. Surya Sejahtera 20"
          pic={Hospital2}
        />
        <ListHospitals
          type="Rumah Sakit"
          name="Tingkatan Paling Atas"
          address="Jln. Surya Sejahtera 20"
          pic={Hospital3}
        />
      </View>
    </View>
  );
};

export default Hospitals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  imageBackground: {
    height: 240,
  },
  contentSection: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 20,
    marginTop: -30,
    paddingTop: 14,
  },
  screenTitle: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.white,
    textAlign: 'center',
    marginTop: 30,
  },
  hospitalCount: {
    fontSize: 14,
    fontFamily: fonts.primary[300],
    color: colors.white,
    textAlign: 'center',
    marginTop: 6,
  },
});
