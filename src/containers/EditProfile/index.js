import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {ScrollView} from 'react-native-gesture-handler';
import {ILPhotoNull} from '../../assets';
import {Button, Gap, Header, InputText, Profile} from '../../components';
import {Fire} from '../../config';
import {colors, getData, storeData} from '../../utils';
import ImagePicker from 'react-native-image-picker';

const EditProfile = ({navigation}) => {
  const [profile, setProfile] = useState({
    fullName: '',
    profession: '',
    email: '',
  });
  const [password, setPassword] = useState('');
  const [photo, setPhoto] = useState(ILPhotoNull);
  const [photoForDb, setPhotoForDb] = useState('');

  useEffect(() => {
    getData('userData').then((res) => {
      const data = res;
      setPhoto({uri: data.photo});
      setProfile(data);
    });
  }, []);

  const update = () => {
    if (password.length > 0) {
      if (password.length < 6) {
        showMessage({
          message: 'Ooops password kurang dari 6 karakter',
          type: 'default',
          backgroundColor: colors.errorMessage,
          color: colors.white,
        });
      } else {
        updatePassword();
        updateDataProfile();
      }
    } else {
      updateDataProfile();
    }
  };

  const updatePassword = () => {
    // new Promise((resolve, reject) => {
    Fire.auth().onAuthStateChanged((user) => {
      if (user) {
        user.updatePassword(password).catch((err) => {
          showMessage({
            message: err.message,
            type: 'default',
            backgroundColor: colors.errorMessage,
            color: colors.white,
          });
        });
      } else {
        navigation.replace('GetStartedScreen');
      }
    });
  };
  const updateDataProfile = () => {
    const data = profile;
    data.photo = photoForDb;
    console.log('check profile => ', data);
    Fire.database()
      .ref(`users/${profile.uid}/`)
      .update(data)
      .then(() => {
        showMessage({
          message: 'Success update data',
          type: 'default',
          backgroundColor: colors.primary,
          color: colors.white,
        });
        storeData('userData', data)
          .then(() => {
            navigation.replace('MainApp');
          })
      })
      .catch((err) => {
        showMessage({
          message: err.message,
          type: 'default',
          backgroundColor: colors.errorMessage,
          color: colors.white,
        });
      });
  };

  const getImage = () => {
    ImagePicker.launchImageLibrary(
      {quality: 0.5, maxHeight: 200, maxWidth: 200},
      (response) => {
        const image = {uri: response.uri};
        if (response.didCancel || response.error) {
          showMessage({
            message: 'Sepertinya anda membatalkan upload Foto ?',
            type: 'default',
            backgroundColor: colors.errorMessage,
            color: colors.white,
          });
        } else {
          const photoForDb = `data:${response.type};base64, ${response.data}`;
          setPhoto(image);
          setPhotoForDb(photoForDb);
        }
      },
    );
  };

  const onChangeText = (key, value) => {
    setProfile({...profile, [key]: value});
  };
  return (
    <View style={styles.container}>
      <Header headerTitle="Edit Profile" onPress={() => navigation.goBack()} />
      <ScrollView>
        <View style={styles.content}>
          <Profile
            isRemove
            avatar={photo}
            icon="removePhoto"
            onPress={getImage}
          />
          <Gap height={26} />
          <InputText
            label="Full Name"
            value={profile.fullName}
            onChangeText={(value) => onChangeText('fullName', value)}
          />
          <Gap height={26} />
          <InputText
            label="Pekerjaan"
            value={profile.profession}
            onChangeText={(value) => onChangeText('profession', value)}
          />
          <Gap height={26} />
          <InputText label="Email Address" value={profile.email} disable />
          <Gap height={26} />
          <InputText
            label="Password"
            value={password}
            onChangeText={(value) => setPassword(value)}
            secureTextEntry
          />
          <Gap height={40} />
          <Button title="Save Profile" onPress={() => update()} />
        </View>
      </ScrollView>
    </View>
  );
};

export default EditProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    padding: 40,
    paddingTop: 0,
  },
});
