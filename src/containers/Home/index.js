import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Doctor1, Doctor2, Doctor3, DummyData} from '../../assets';
import {
  Gap,
  HomeCategory,
  HomeProfile,
  NewsItem,
  RatedDoctor,
} from '../../components';
import {colors, fonts, showError} from '../../utils';
import {Fire} from '../../config';

const Home = ({navigation}) => {
  const [news, setNews] = useState(null);
  const [doctorCategory, setDoctorCategory] = useState(null);
  const [topRated, setTopRated] = useState(null);
  const getNews = () => {
    Fire.database()
      .ref('news/')
      .once('value')
      .then((res) => {
        if (res.val()) {
          setNews(res.val());
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  const getTopDoctor = () => {
    Fire.database()
      .ref('doctors/')
      .orderByChild('rate')
      .limitToLast(3)
      .once('value')
      .then((res) => {
        if (res.val()) {
          const oldData = res.val();
          let data = [];
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key],
            });
          });
          setTopRated(data);
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  const getCategory = () => {
    Fire.database()
      .ref('category/')
      .once('value')
      .then((res) => {
        if (res.val()) {
          setDoctorCategory(res.val());
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  useEffect(() => {
    getNews();
    getCategory();
    getTopDoctor();
  }, []);

  console.log('check value news => ', {news, doctorCategory, topRated});

  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Gap height={30} />
          <View style={styles.sectionWrapper}>
            <HomeProfile onPress={() => navigation.navigate('UserProfile')} />
            <Text style={styles.welcomeText}>
              Mau konsultasi dengan siapa hari ini?
            </Text>
          </View>
          <View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View style={styles.categoryWrapper}>
                <Gap width={16} />
                {doctorCategory &&
                  doctorCategory.map((data) => {
                    return (
                      <HomeCategory
                        key={data.id}
                        category={data.category}
                        onPress={() => navigation.navigate('ChooseDoctor')}
                      />
                    );
                  })}
                <Gap width={6} />
              </View>
            </ScrollView>
          </View>
          <View style={styles.sectionWrapper}>
            <Text style={styles.sectionText}>Top Rated Doctors</Text>
            {topRated &&
              topRated.map((value) => {
                return (
                  <RatedDoctor
                    key={value.id}
                    name={value.data.fullName}
                    desc={value.data.profession}
                    onPress={() => navigation.navigate('DoctorProfile')}
                    avatar={{uri : value.data.photo}}
                    rate={value.data.rate}
                  />
                );
              })}
            <Text style={styles.sectionText}>Good News</Text>
          </View>
          {news &&
            news.map((items) => {
              return (
                <NewsItem
                  key={items.id}
                  title={items.title}
                  date={items.date}
                  image={items.image}
                />
              );
            })}
          <Gap height={30} />
        </ScrollView>
      </View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  contentContainer: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  sectionWrapper: {
    paddingHorizontal: 16,
  },
  welcomeText: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 30,
    maxWidth: 209,
  },
  sectionText: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    marginTop: 30,
    marginBottom: 16,
  },
  categoryWrapper: {
    flexDirection: 'row',
    marginTop: 16,
  },
});
