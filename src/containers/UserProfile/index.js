import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {ILPhotoNull} from '../../assets';
import {Gap, Header, ListItem, Profile} from '../../components';
import {Fire} from '../../config';
import {colors, getData} from '../../utils';

const UserProfile = ({navigation}) => {
  const [profile, setProfile] = useState({
    fullName: '',
    profession: '',
    photo: ILPhotoNull,
  });

  useEffect(() => {
    getData('userData').then((res) => {
      const data = res;
      data.photo = {uri: data.photo};
      setProfile(data);
    });
  }, []);

  const signOut = () => {
    Fire.auth()
      .signOut()
      .then(() => {
        navigation.replace('GetStartedScreen');
      })
      .catch((err) => {
        showMessage({
          message: err.message,
          type: 'default',
          backgroundColor: colors.errorMessage,
          color: colors.white,
        });
      });
  };
  return (
    <View style={styles.container}>
      <Header headerTitle="User Profile" onPress={() => navigation.goBack()} />
      <Gap height={10} />
      {profile.fullName.length > 0 && (
        <Profile
          name={profile.fullName}
          desc={profile.profession}
          avatar={profile.photo}
        />
      )}

      <Gap height={14} />
      <ListItem
        title="Edit Profile"
        desc="Last updated yesterday"
        type="next"
        icon="editProfile"
        onPress={() => navigation.navigate('EditProfile')}
      />
      <ListItem
        title="Language"
        desc="Available 12 languages"
        type="next"
        icon="language"
      />
      <ListItem
        title="Give Us Rate"
        desc="On Google Play Store"
        type="next"
        icon="giveRate"
      />
      <ListItem
        title="Help Center"
        desc="Read our guidelines"
        type="next"
        icon="helpCenter"
      />
      <ListItem
        title="Log Out"
        desc="Read our guidelines"
        type="next"
        icon="helpCenter"
        onPress={signOut}
      />
    </View>
  );
};

export default UserProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
