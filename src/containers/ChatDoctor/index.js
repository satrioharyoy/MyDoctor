import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ChatText, Header, InputMessage} from '../../components';
import {colors, fonts} from '../../utils';

const ChatDoctor = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header type="darkProfile" onPress={()=>navigation.goBack()}  />
      <View style={styles.content}>
        <Text style={styles.dateText}>Senin, 21 Maret, 2020</Text>
        <ChatText isMe />
        <ChatText />
        <ChatText isMe />
      </View>
      <InputMessage />
    </View>
  );
};

export default ChatDoctor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    flex: 1,
  },
  dateText: {
    fontSize: 11,
    fontFamily: fonts.primary[400],
    paddingVertical: 20,
    textAlign: 'center',
    color: colors.text.secondary,
  },
});
