import SplashScreen from './SplashScreen';
import GetStarted from './GetStarted';
import SignIn from './SignIn';
import SignUp from './SignUp';
import UploadPhoto from './UploadPhoto';
import Home from './Home';
import Messages from './Messages';
import Hospitals from './Hospitals';
import ChooseDoctor from './ChooseDoctor';
import ChatDoctor from './ChatDoctor';
import UserProfile from './UserProfile';
import EditProfile from './EditProfile';
import DoctorProfile from './DoctorProfile';

export {
  SplashScreen,
  GetStarted,
  SignIn,
  SignUp,
  UploadPhoto,
  Home,
  Messages,
  Hospitals,
  ChooseDoctor,
  ChatDoctor,
  UserProfile,
  EditProfile,
  DoctorProfile,
};
