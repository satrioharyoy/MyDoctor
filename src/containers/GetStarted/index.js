import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { BGGetStarted, ILLogo } from '../../assets';
import { Button, Gap } from '../../components';
import { fonts } from '../../utils';

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={BGGetStarted} style={styles.pageContainer}>
      <View>
        <ILLogo />
        <Text style={styles.Title}>
          Konsultasi dengan dokter jadi lebih mudah & fleksibel
        </Text>
      </View>
      <View>
        <Button
          title={'Get Started'}
          onPress={() => navigation.navigate('SignUpScreen')}
        />
        <Gap height={16} />
        <Button
          type={'secondary'}
          title={'Sign In'}
          onPress={() => navigation.navigate('SignInScreen')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  pageContainer: {
    padding: 40,
    flex: 1,
    justifyContent: 'space-between',
  },
  Title: {
    fontSize: 28,
    color: 'white',
    marginTop: 91,
    fontFamily: fonts.primary[600],
  },
});
