import React, {useState} from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import ImagePicker from 'react-native-image-picker';
import {IconAddPhoto, IconRemovePhoto, ILPhotoNull} from '../../assets';
import {Button, Gap, Link} from '../../components';
import Header from '../../components/molecules/Header';
import {Fire} from '../../config';
import {colors, fonts, storeData} from '../../utils';

const UploadPhoto = ({navigation, route}) => {
  const [hasPhoto, sethasPhoto] = useState(false);
  const [photo, setPhoto] = useState(ILPhotoNull);
  const [photoForDb, setPhotoForDb] = useState('');
  const {fullName, profession, uid} = route.params;
  console.log('check uid => ', uid);
  const getPhoto = () => {
    ImagePicker.launchImageLibrary(
      {quality: 0.5, maxHeight: 200, maxWidth: 200},
      (response) => {
        const image = {uri: response.uri};
        console.log('get data ', response);
        if (response.didCancel || response.error) {
          showMessage({
            message: 'Sepertinya anda membatalkan upload Foto ?',
            type: 'default',
            backgroundColor: colors.errorMessage,
            color: colors.white,
            duration: 40000,
          });
        } else {
          const photoForDb = `data:${response.type};base64, ${response.data}`;
          setPhoto(image);
          sethasPhoto(true);
          setPhotoForDb(photoForDb);
        }
      },
    );
  };

  const doUploadPhoto = () => {
    Fire.database().ref(`users/${uid}/`).update({photo: photoForDb});
    let data = route.params
    data.photo = photoForDb
    storeData('userData', data);
    navigation.replace('MainApp');
  };

  return (
    <View style={styles.container}>
      <Header
        headerTitle="Upload Photo"
        icon="iconBackDark"
        onPress={() => navigation.goBack()}
      />
      <View style={styles.contentContainer}>
        <View style={styles.contentProfile}>
          <TouchableOpacity onPress={getPhoto} style={styles.avatarWrapper}>
            <Image style={styles.avatar} source={photo} />
            {hasPhoto ? (
              <IconRemovePhoto style={styles.btnAddPhoto} />
            ) : (
              <IconAddPhoto style={styles.btnAddPhoto} />
            )}
          </TouchableOpacity>
          <Text style={styles.nameProfile}>{fullName}</Text>
          <Text style={styles.jobProfile}>{profession}</Text>
        </View>
        <View>
          <Button
            title="Upload and Continue"
            onPress={doUploadPhoto}
            disable={!hasPhoto}
          />
          <Gap height={30} />
          <Link
            title="Skip for this"
            fontSize={16}
            textAlign="center"
            onPress={() => navigation.replace('MainApp')}
          />
        </View>
      </View>
    </View>
  );
};

export default UploadPhoto;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },
  contentContainer: {
    paddingHorizontal: 40,
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: 64,
  },
  contentProfile: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarWrapper: {
    width: 130,
    height: 130,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 130 / 2,
    borderColor: colors.borders,
  },
  avatar: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  btnAddPhoto: {
    position: 'absolute',
    bottom: 8,
    right: 6,
  },
  nameProfile: {
    fontSize: 24,
    fontFamily: fonts.primary[600],
    marginTop: 26,
    textAlign: 'center',
  },
  jobProfile: {
    fontSize: 18,
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
    marginTop: 4,
  },
});
