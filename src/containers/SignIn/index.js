import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {ILLogo} from '../../assets';
import {Button, Gap, InputText, Link} from '../../components';
import {Fire} from '../../config';
import {colors, fonts, showError, storeData, useForm} from '../../utils';

const SignIn = ({navigation}) => {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });
  const dispatch = useDispatch();
  const doLogin = () => {
    dispatch({type: 'SET_LOADING', value: true});
    Fire.auth()
      .signInWithEmailAndPassword(form.email, form.password)
      .then((res) => {
        dispatch({type: 'SET_LOADING', value: false});
        Fire.database()
          .ref(`users/${res.user.uid}/`)
          .once('value')
          .then((resDB) => {
            storeData('userData', resDB.val());
            setForm('reset');
            navigation.replace('MainApp');
          });
      })
      .catch((err) => {
        dispatch({type: 'SET_LOADING', value: false});
        setForm('reset');
        showError(err.message);
      });
    setTimeout(() => {
      dispatch({type: 'SET_LOADING', value: false});
    }, 50000);
  };
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Gap height={40} />
        <ILLogo />
        <Text style={styles.title}>Masuk dan mulai berkonsultasi</Text>
        <InputText
          label="Email Address"
          value={form.email}
          onChangeText={(value) => setForm('email', value)}
        />
        <Gap height={24} />
        <InputText
          label="Password"
          value={form.password}
          onChangeText={(value) => setForm('password', value)}
          secureTextEntry
        />
        <Gap height={10} />
        <Link title="Forgot My Password" />
        <Gap height={40} />
        <Button title="Sign In" onPress={doLogin} />
        <Gap height={30} />
        <Link
          title="Create New Account"
          fontSize={16}
          textAlign="center"
          onPress={() => navigation.navigate('SignUpScreen')}
        />
      </ScrollView>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  container: {
    padding: 40,
    backgroundColor: 'white',
    flex: 1,
  },
  title: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    marginVertical: 40,
    color: colors.text.primary,
    maxWidth: 153,
  },
});
