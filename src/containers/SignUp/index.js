import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {ScrollView} from 'react-native-gesture-handler';
import {Button, Gap, Header, InputText, Loading} from '../../components';
import {Fire} from '../../config';
import {colors, useForm} from '../../utils';

const SignUp = ({navigation}) => {
  const [form, setForm] = useForm({
    fullName: '',
    profession: '',
    email: '',
    password: '',
  });

  const [loading, setLoading] = useState(false);

  const doOnSubmit = () => {
    setLoading(true);
    console.log('create')
    Fire.auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then((success) => {
        const data = {
          fullName: form.fullName,
          profession: form.profession,
          email: form.email,
          uid: success.user.uid,
        };
        setLoading(false);
        Fire.database().ref(`users/${success.user.uid}/`).set(data);
        setForm('reset');
        navigation.navigate('UploadPhotoScreen', data);
      })
      .catch((error) => {
        // Handle Errors here.
        const errorMessage = error.message;
        setLoading(false);
        showMessage({
          message: errorMessage,
          type: 'default',
          backgroundColor: colors.errorMessage,
          color: colors.white,
        });
      });
    setTimeout(() => {
      setLoading(false)
    }, 10000);
  };

  return (
    <>
      <View style={styles.container}>
        <Header
          headerTitle="Daftar Akun"
          icon="iconBackDark"
          onPress={() => navigation.goBack()}
        />
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <InputText
              label="Full Name"
              value={form.fullName}
              onChangeText={(value) => setForm('fullName', value)}
            />
            <Gap height={24} />
            <InputText
              label="Pekerjaan"
              value={form.profession}
              onChangeText={(value) => setForm('profession', value)}
            />
            <Gap height={24} />
            <InputText
              label="Email Address"
              value={form.email}
              onChangeText={(value) => setForm('email', value)}
            />
            <Gap height={24} />
            <InputText
              label="Password"
              value={form.password}
              onChangeText={(value) => setForm('password', value)}
              secureTextEntry
            />
            <Gap height={40} />
            <Button title="Continue" onPress={() => doOnSubmit()} />
          </ScrollView>
        </View>
      </View>
      {loading && <Loading />}
    </>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  content: {
    padding: 40,
    paddingTop: 0,
  },
});
