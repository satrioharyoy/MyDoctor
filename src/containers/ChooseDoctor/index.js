import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Doctor1, Doctor2, Doctor3} from '../../assets';
import {Header, ListItem} from '../../components';
import {colors} from '../../utils';

const ChooseDoctor = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header
        headerTitle="Dokter Umum"
        type="dark"
        onPress={() => navigation.goBack()}
        icon="iconBackLight"
      />
      <View style={styles.contentSection}>
        <ListItem
          title="Alexander Jannie"
          profile={Doctor1}
          desc="Wanita"
          type="next"
          onPress = {()=> navigation.navigate('ChatDoctor')}
        />
        <ListItem
          title="John McParker Steve"
          profile={Doctor2}
          desc="Pria"
          type="next"
        />
        <ListItem
          title="Nairobi Putri Hayza"
          profile={Doctor3}
          desc="Wanita"
          type="next"
        />
      </View>
    </View>
  );
};

export default ChooseDoctor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  contentSection: {
    marginTop: 20,
  },
});
