import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Doctor3} from '../../assets';
import {Button, Gap, Header, Profile, ProfileItem} from '../../components';
import {colors} from '../../utils';

const DoctorProfile = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header headerTitle="Profile" onPress={()=>navigation.goBack()}  />
      <Gap height={24} />
      <Profile
        name="Nairobi Putri Hayza"
        desc="Dokter Anak"
        avatar={Doctor3}
        icon="female"
      />
      <Gap height={10} />
      <ProfileItem label="Alumnus" value="Universitas Indonesia, 2020" />
      <ProfileItem label="Tempat Praktik" value="Rumah Sakit Umum, Bandung" />
      <ProfileItem label="No. STR" value="0000116622081996" />
      <View style={styles.buttonWrapper}>
        <Button title="Start Consultation" onPress={()=>navigation.navigate('ChatDoctor')}  />
      </View>
    </View>
  );
};

export default DoctorProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  buttonWrapper: {
    paddingTop: 23,
    paddingHorizontal: 40,
  },
});
