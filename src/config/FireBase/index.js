import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyAJYwvoGryXuMj1G8nga-9XoW4j7HO_M4w",
    authDomain: "mydoctor-31b14.firebaseapp.com",
    databaseURL: "https://mydoctor-31b14.firebaseio.com",
    projectId: "mydoctor-31b14",
    storageBucket: "mydoctor-31b14.appspot.com",
    messagingSenderId: "317249019431",
    appId: "1:317249019431:web:b980b14ca95c11f6fb3802",
    measurementId: "G-Q2WT02L34W"
  })

  const Fire = firebase

  export default Fire

  